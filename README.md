# THOTH
A rewrite of an old project using [Laravel](https://www.laravel.com/) and [VueJs](https://www.vuejs.org)

The idea is to digitize every paper document sending them as attachments to an email address. Once tagged the user can search them using the power of [elasticsearch](https://www.elastic.co)

## Note
This is a [meta](https://github.com/mateodelnorte/meta) project including the [backend](https://gitlab.com/acrippa/thoth-laravel.git), the [frontend](https://gitlab.com/acrippa/thoth-vue.git), and the [service](https://gitlab.com/acrippa/thoth-service.git) that handles the email